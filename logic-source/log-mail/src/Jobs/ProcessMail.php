<?php

namespace LogicSource\LogMail\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $message;
    protected $trace;
    protected $datetime;


    public function __construct($message, $trace, $datetime)
    {
        $this->message = $message;
        $this->trace = $trace;
        $this->datetime = $datetime;
    }

    public function handle()
    {
        try {
            \Mail::to(explode(',', env('MAIL_ADMIN')))->send(new LogMail(
                $this->message,
                $this->datetime ? $datetime->format('Y-m-d H:i:s') : null,
                $this->trace
            ));
        } catch (\Throwable $e) {
            \Log::info($e->getTraceAsString());
        }
    }

    public function failed(\Exception $exception)
    {

    }
}
