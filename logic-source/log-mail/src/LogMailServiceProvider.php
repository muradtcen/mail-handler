<?php

namespace LogicSource\LogMail;

use Illuminate\Support\ServiceProvider;
use Log;
use LogicSource\LogMail\Mail\LogMailHandler;

class LogMailServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views/', 'LogMail');

        if (env('APP_MAIL_LOG_LEVEL') and
            env('MAIL_USERNAME') and env('MAIL_ADMIN')) {

            Log::getMonolog()->pushHandler(new LogMailHandler(env('APP_MAIL_LOG_LEVEL')));
        }
    }

}