<div>
    Произошла ошибка в приложении {{env('APP_NAME')}} по адресу {{env('APP_URL')}}
    <br>
    Что: {{ $content }}
    <br>
    Когда: {{ $dateTime }}
    <br>
    Трассировка:
    @foreach($traceArray as $traceItem)
        {{$traceItem}}<br>
    @endforeach
</div>