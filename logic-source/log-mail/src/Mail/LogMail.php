<?php

namespace LogicSource\LogMail\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class LogMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $content;
    public $dateTime;
    public $traceArray;
    public $tries = 3;

    /**
     * LogMail constructor.
     * @param $message
     * @param $dateTime
     * @param $trace
     */
    public function __construct(string $message, string $dateTime, string $trace)
    {
        $this->content = $message;
        $this->dateTime = $dateTime;
        $this->traceArray = explode('#', $trace);
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'))->view('LogMail::log-mail');
    }
}