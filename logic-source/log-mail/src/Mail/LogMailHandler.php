<?php

namespace LogicSource\LogMail\Mail;


use LogicSource\LogMail\Jobs\ProcessMail;
use Monolog\Handler\AbstractHandler;

class LogMailHandler extends AbstractHandler
{
    public function handle($record)
    {
        $e = array_get($record, 'context.exception');

        $message = !\App::runningInConsole()
            ? array_get($record, 'message') . ' Request path: ' . \Request::path()
            : array_get($record, 'message') . " через консоль";


        $trace = $e ? $e->getTraceAsString() : '';
        $datetime = array_get($record, 'datetime');

        ProcessMail::dispatch($message, $trace, $datetime);
    }
}